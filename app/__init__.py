from flask import Flask, Blueprint
from flask.ext.mongoengine import MongoEngine
from flask.ext.bootstrap import Bootstrap
from flask.ext.login import LoginManager

app = Flask(__name__)

db = MongoEngine(app)
bootstrap = Bootstrap(app)
lm = LoginManager(app)
lm.login_view = 'creator.login'

app.config["MONGODB_SETTINGS"] = {'DB': "soc_poll"}
app.config["SECRET_KEY"] = "KeepThisS3cr3t"

from creator.creatorLogin import creator as creator_blueprint
app.register_blueprint(creator_blueprint)

from creator.organizationPage import organization_blueprint
app.register_blueprint(organization_blueprint)

if __name__ == '__main__':
	app.secret_key = 'mysecret'
	app.run()
