from app import db, lm
from werkzeug.security import generate_password_hash, check_password_hash
from flask.ext.login import UserMixin


class Creator(UserMixin ,db.Document):
    username = db.StringField()
    password_hash = db.StringField()

    def set_password(self, password):
        self.password_hash = generate_password_hash(password=password)

    def verify_password(self, password):
        return check_password_hash(password=password, pwhash=self.password_hash)

    @staticmethod
    def register(username, password):
        new_user = Creator(username=username)
        new_user.set_password(password)
        new_user.save()
        return new_user

        

@lm.user_loader
def load_user(id):
    return Creator.objects(id=unicode(id)).first()