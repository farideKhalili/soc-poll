from flask import Flask, render_template, redirect, url_for, flash, request
from flask.ext.login import login_user, logout_user, login_required
from .models import Creator as myCreator
from . import creator
from .forms import CreatorLogin, CreatorSignup

templates_path = 'creatorLogin/'

@creator.route('/creator/login', methods=['GET', 'POST'])
def login():
	form = CreatorLogin()
	if form.validate_on_submit():
		user = myCreator.objects(username=form.username.data).first()
		if user == None or not user.verify_password(form.password.data):
			return redirect(url_for('creator.login'))
		login_user(user, form.remember_me.data)
		return redirect(request.args.get('next') or url_for('creator.main'))
	return render_template('%screator_login.html' % templates_path, form=form)


@creator.route('/creator/logout')
@login_required
def logout():
	logout_user()
	return render_template('%screator_logged_out.html' % templates_path)


@creator.route('/creator/signup', methods=['GET', 'POST'])
def signup():
	warn = ''
	form = CreatorSignup()
	if form.validate_on_submit():
		if myCreator.objects(username=form.username.data): # empty list acts like false
			warn = 'username has been taken. please choose another one.'
			return redirect(url_for('creator.signup'))
		myCreator().register(username=form.username.data, password=form.password.data)
		return render_template('%saccount_created.html' % templates_path)
	return render_template('%screator_signup.html' % templates_path, form=form)

@creator.route('/creator/main')
@login_required
def main():
	return render_template('%screator_logged_in.html' % templates_path)