from app import db


class Organization(db.Document):
	name = db.StringField(required=True)
	votebox_list=db.ListField(db.ReferenceField('Votebox'))
	@staticmethod
	def create_votebox(name):
		new_votebox = votebox.create_votebox(name, self)


class votebox(db.Document):
	name=db.StringField(required=True)
	organization_id=db.ReferenceField('Organization', required=True)
	
	@staticmethod
	def create_votebox(name, organization_id):
		new_votebox = votebox(name=name , organization_id=organization_id)
		new_votebox.save()
		organization_id.votebox_list.append(new_votebox)
