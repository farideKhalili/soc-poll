from flask import Flask, render_template, url_for,redirect
#from flask_login import login_required
from app import db
from . import organization_blueprint
from flask.views import MethodView
from app.creator.voteBoxPage.models import VoteBox
from .models import Organization

#@login_required(login_url=url_for('creator.creatorLogin.creator_login'))
class create_votebox(MethodView):
	def get(self,org_name):
		org = Organization.objects(name=org_name).first()
		return redirect('/pargolsPage')
		#return render_template('createvotebox.html')


#@login_required(login_url=url_for('creator.creatorLogin.creator_login'))
class show_result(MethodView):
	def get(self,id,org_name):
		org = Organization.objects(name=org_name).first()
		boxes = VoteBox.objects.get_or_404(id=id)
		winner = boxes.candidates[0]
		total_count = 0
		for candidate in boxes.candidates:
			total_count += candidate.vote_count
			if candidate.vote_count > winner.vote_count:
				winner = candidate
		return render_template('voteBoxPage/FinalResult.html',boxes=boxes,winner=winner,now=datetime.now(),total_count=total_count,loggedIn=loggedIn)

class show_all(MethodView):
	def get(self,org_name):
		org = Organization.objects(name=org_name).first()
		#if org==None
			#return '<h1>such an organization doesn't exist</h1>'
		return render_template('organizationPage/organ.html',org=org)

	#return render_template('organizationPage/organ.html',Organization=Organization)
	#return '<h1>it works</h1>'


organization_blueprint.add_url_rule('/organization/<org_name>/create',view_func=create_votebox.as_view('createNewVotebox'))
organization_blueprint.add_url_rule('/organization/<org_name>/votebox/<id>/result',view_func=show_result.as_view('showResults'))
organization_blueprint.add_url_rule('/organization/<org_name>',view_func=show_all.as_view('organizationPage'))
