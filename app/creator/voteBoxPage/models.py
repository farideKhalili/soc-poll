from flask import Flask
from flask_mongoengine import MongoEngine
import datetime
from flask import url_for
from app import db


class Candidate(db.EmbeddedDocument):
	name = db.StringField(verbose_nam="cName",required=True)
	vote_count = db.IntField()
	
	
class VoteBox(db.Document):
	created_at = db.DateTimeField(default=datetime.datetime.now, required=True)
	name = db.StringField(verbose_name="Name",required=True)
	description = db.StringField(verbose_nam="Descripton",required=True)
	candidates = db.ListField(db.EmbeddedDocumentField('Candidate'))
	start = db.DateTimeField()
	end = db.DateTimeField()




