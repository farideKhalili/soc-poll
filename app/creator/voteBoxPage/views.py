from flask import Blueprint, request, redirect, render_template, url_for
from flask.views import MethodView
from app.voter.voteBoxPage.models import VoteBox, Candidate
from operator import attrgetter
from datetime import datetime
from flask_mongoengine import MongoEngine
from flask_login import login_required
from flask.ext.login import current_user

boxes = Blueprint('boxes',__name__,template_folder='templates')


class BoxView(MethodView):
	def get(self, id):
	 	boxes = VoteBox.objects.get_or_404(id=id)
	 	return render_template('voteBoxPage/box.html',boxes=boxes,id=id,now=datetime.now())


class FinalResultView(MethodView):

	def get(self,id):
		#max(self.allPartners, key=attrgetter('attrOne'))
		loggedIn = False 
		if current_user.is_authenticated:
			loggedIn = True
		boxes = VoteBox.objects.get_or_404(id=id)
		winner = boxes.candidates[0]
		total_count = 0
		for candidate in boxes.candidates:
			total_count += candidate.vote_count
			if candidate.vote_count > winner.vote_count:
				winner = candidate
		return render_template('voteBoxPage/FinalResult.html',boxes=boxes,winner=winner,now=datetime.now(),total_count=total_count,loggedIn=loggedIn)

class MomentaryResultView(MethodView):

	def post(self,id):
		boxes = VoteBox.objects.get_or_404(id=id)
		return render_template('MomentaryResult.html',boxes=boxes)




boxes.add_url_rule('/<id>',view_func=BoxView.as_view('box'))
boxes.add_url_rule('/<id>/result',view_func=FinalResultView.as_view('FinalResult'))
boxes.add_url_rule('/<id>/MomentaryResult',view_func=FinalResultView.as_view('MomentaryResult'))


